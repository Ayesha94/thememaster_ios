//
//  ViewController.swift
//  ThemeMaster
//
//  Created by Macbook on 1/24/30 H.
//  Copyright © 30 Clustox. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "ThemeMaster"
        self.configureLayout()
        
        self.view.layoutIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    public func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            print("Error Occurred")
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func configureLayout() {
        
        //MAIN_VIEW_BACKGROUND_COLOR
        guard let backgroundColor = UserDefaults.standard.string(forKey: Constants.MAIN_VIEW_BACKGROUND_COLOR_KEY) else {
            return
        }
        let viewcolor = hexStringToUIColor(hex: backgroundColor)
        self.viewHome.backgroundColor = viewcolor
        
        //NAVIGATION_BAR_COLOR
        guard let navigationColor = UserDefaults.standard.string(forKey: Constants.NAVIGATION_BAR_COLOR_KEY) else {
            return
        }
        let navigation = hexStringToUIColor(hex: navigationColor)
        self.navigationController?.navigationBar.barTintColor = navigation
        
        //NAVIGATION_TEXT_COLOR
        guard let navTextColor = UserDefaults.standard.string(forKey: Constants.NAVIGATION_TITLE_COLOR_KEY) else {
            return
        }
        let nTextColor = hexStringToUIColor(hex: navTextColor)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: nTextColor]
        
        //BUTTON_BACKGROUND_COLOR
        guard let buttonBackground = UserDefaults.standard.string(forKey: Constants.BUTTON_BACKGROUND_COLOR_KEY) else {
            return
        }
        let buttoncolor = hexStringToUIColor(hex: buttonBackground)
        self.button.backgroundColor = buttoncolor
        
        //BUTTON_TEXT_COLOR
        guard let buttonTextC = UserDefaults.standard.string(forKey: Constants.BUTTON_TEXT_COLOR_KEY) else {
            return
        }
        let buttonTextColor = hexStringToUIColor(hex: buttonTextC)
        self.button.setTitleColor(buttonTextColor, for: .normal)
        
        //LABEL_TEXT_COLOR
        guard let labelText = UserDefaults.standard.string (forKey: Constants.LABEL_TEXT_COLOR_KEY) else {
            return
        }
        let label = hexStringToUIColor(hex: labelText)
        self.label.textColor = label
        
    }
}

