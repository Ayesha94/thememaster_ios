import UIKit
import OneSignal
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    public typealias JSON = Any
    public typealias JSONDictionary = [String: JSON]
    public typealias JSONArray = [JSON]


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            print("Received Notification: \(notification!.payload.notificationID)")
            
        }
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload = result!.notification.payload
            
            if let additionalData = payload.additionalData {
                if payload.title != nil {
                    let messageTitle = payload.title
                    print("Message Title = \(messageTitle!)")
                    
                    
                    if let backgroundColor = self.JSONToStringIfNull(element: additionalData[Constants.MAIN_VIEW_BACKGROUND_COLOR_KEY]) {
                        
                        UserDefaults.standard.set(backgroundColor, forKey: Constants.MAIN_VIEW_BACKGROUND_COLOR_KEY)
                        UserDefaults.standard.synchronize()
                    }
                    if let buttonBackgroundColor = self.JSONToStringIfNull(element: additionalData[Constants.BUTTON_BACKGROUND_COLOR_KEY]) {
                        
                        UserDefaults.standard.set(buttonBackgroundColor, forKey: Constants.BUTTON_BACKGROUND_COLOR_KEY)
                        UserDefaults.standard.synchronize()
                    }
                    if let navigationBarColor = self.JSONToStringIfNull(element: additionalData[Constants.NAVIGATION_BAR_COLOR_KEY]) {
                        
                        UserDefaults.standard.set(navigationBarColor, forKey: Constants.NAVIGATION_BAR_COLOR_KEY)
                        UserDefaults.standard.synchronize()
                    }
                    if let labelTextColor = self.JSONToStringIfNull(element: additionalData[Constants.LABEL_TEXT_COLOR_KEY]) {
                        
                        UserDefaults.standard.set(labelTextColor, forKey: Constants.LABEL_TEXT_COLOR_KEY)
                        UserDefaults.standard.synchronize()
                    }
                    if let buttonTextColor = self.JSONToStringIfNull(element: additionalData[Constants.BUTTON_TEXT_COLOR_KEY]) {
                        
                        UserDefaults.standard.set(buttonTextColor, forKey: Constants.BUTTON_TEXT_COLOR_KEY)
                        UserDefaults.standard.synchronize()
                    }
                    if let navigationTextColor = self.JSONToStringIfNull(element: additionalData[Constants.NAVIGATION_TITLE_COLOR_KEY]) {
                        
                        UserDefaults.standard.set(navigationTextColor, forKey: Constants.NAVIGATION_TITLE_COLOR_KEY)
                        UserDefaults.standard.synchronize()
                    }
                    
                }
            }
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: true,
                                     kOSSettingsKeyInAppLaunchURL: false]
        
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: Constants.ONE_SIGNAL_APP_ID,
                                        handleNotificationReceived: notificationReceivedBlock,
                                        handleNotificationAction: notificationOpenedBlock,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        
        // Call when you want to prompt the user to accept push notifications.
        // Only call once and only if you set kOSSettingsKeyAutoPrompt in AppDelegate to false.
        OneSignal.add(self as OSPermissionObserver)
        OneSignal.add(self as OSSubscriptionObserver)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        print("App is in background")
        let url = "http://54.204.54.16/api/v1/branch?latitude=31.5108627&longitude=74.3503777"
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response) in
            
            switch response.result {
            case .success( _):
                print("CALL SUCCESSFUL")
            case .failure(_):
                print("CALL FAILURE")
            }
        }
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    public func JSONToStringIfNull(element: JSON?) -> String? {
        guard let elementString = element as? String else {
            return ""
        }
        return elementString
    }

}

extension AppDelegate: OSSubscriptionObserver {
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            // The user is subscribed
            // Either the user subscribed for the first time
            // Or the user was subscribed -> unsubscribed -> subscribed
            let osPlayerId = stateChanges.to.userId
            //Cache.sharedCache.playerId = osPlayerId
            debugPrint("OneSignal player id = \(String(describing: (osPlayerId)))")
        }
    }
}

extension AppDelegate: OSPermissionObserver {
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        // Example of detecting answering the permission prompt
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                print("Thanks for accepting notifications!")
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                print("Notifications not accepted. You can turn them on later under your iOS settings.")
            }
        }
        // prints out all properties
        print("PermissionStateChanges: \n\(stateChanges)")
    }
}

