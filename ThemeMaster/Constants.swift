//
//  Constants.swift
//  ThemeMaster
//
//  Created by Macbook on 1/24/30 H.
//  Copyright © 30 Clustox. All rights reserved.
//

import UIKit

class Constants {
    //Mark - Keys
    static let MAIN_VIEW_BACKGROUND_COLOR_KEY = "main_view_background_color"
    static let NAVIGATION_BAR_COLOR_KEY = "navigation_bar_color"
    static let NAVIGATION_TITLE_COLOR_KEY = "navigation_title_color"
    static let BUTTON_BACKGROUND_COLOR_KEY = "button_background_color"
    static let BUTTON_TEXT_COLOR_KEY = "button_text_color"
    static let LABEL_TEXT_COLOR_KEY = "label_text_color"
    
    
    
    //Mark - Values
    static let DEFAULT_VIEW_BACKGROUND_COLOR = "A8A09E"
    static let DEFAULT_NAVIGATIONBAR_COLOR = "0033cc"
    static let DEFAULT_BUTTON_BACKGROUND_COLOR = ""
    static let DEFAULT_LABEL_TEXT_COLOR = ""
    static let DEFAULT_BUTTON_TEXT_COLOR = ""
    static let DEFAULT_NAVIGATION_TITLE_COLOR = ""
    
    //MARK - Credentials
    static let ONE_SIGNAL_APP_ID = "30369a12-03a0-4d88-90ac-b4c9cc9e9b8d"
}
